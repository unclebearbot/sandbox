package cgroups

type Resource struct {
	CPUSet string
	CPU    string
	Memory string
}
