.PHONY: build clean
ROOT := $(if $(filter-out Windows_NT,$(OS)),$(CURDIR),$(subst \,/,$(shell cmd /c cd)))
BIN := $(or $(ROOT),.)/bin
APP := $(shell basename "$(CURDIR)")$(if $(filter-out Windows_NT,$(OS)),,.exe)
EXEC := $(BIN)/$(APP)
ARGS := -gcflags=-trimpath=$(ROOT) -asmflags=-trimpath=$(ROOT) -a -v
LDFLAGS := -X=main.Version=$(notdir $(shell git describe --all --tags)) -X=main.Build=$(shell git rev-parse --short=8 HEAD)
SRC := $(wildcard *.go)
build:
	mkdir -p $(BIN)
	go mod tidy
	go mod download
	go mod verify
	go generate $(SRC)
	GOBIN=$(BIN) $(env) go build $(ARGS) $(args) -o $(EXEC) -ldflags "$(LDFLAGS) $(ldflags)" $(SRC)
	chmod +x $(EXEC)
clean:
	go clean
	rm -rf $(BIN)
