package cgroups

type cpuSetSubsystem struct{}

func (cpuSetSubsystem cpuSetSubsystem) name() string {
	return "cpuset"
}

func (cpuSetSubsystem cpuSetSubsystem) set(cgroup string, resource Resource) error {
	set(cpuSetSubsystem, cgroup, "cpuset.mems", Resource{Memory: "0"}, "Memory")
	return set(cpuSetSubsystem, cgroup, "cpuset.cpus", resource, "CPUSet")
}

func (cpuSetSubsystem cpuSetSubsystem) apply(cgroup string, pid int) error {
	return apply(cpuSetSubsystem, cgroup, pid)
}

func (cpuSetSubsystem cpuSetSubsystem) remove(cgroup string) error {
	return remove(cpuSetSubsystem, cgroup)
}
