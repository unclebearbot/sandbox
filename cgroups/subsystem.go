package cgroups

import (
	"bufio"
	"errors"
	"io/ioutil"
	"log"
	"os"
	"path"
	"reflect"
	"strconv"
	"strings"
)

type subsystem interface {
	name() string
	set(cgroup string, resource Resource) error
	apply(cgroup string, pid int) error
	remove(cgroup string) error
}

func set(subsystem subsystem, cgroup string, subsystemCgroup string, resource Resource, resourceType string) error {
	subsystemCgroupPath, error := getCgroupPath(subsystem.name(), cgroup, true)
	if error != nil {
		log.Println(error)
		return error
	}
	value := getField(resource, resourceType).String()
	file := path.Join(subsystemCgroupPath, subsystemCgroup)
	if e := ioutil.WriteFile(file, []byte(value), 0644); e != nil {
		log.Println(e)
		return e
	}
	return nil
}

func getField(object interface{}, field string) reflect.Value {
	return reflect.Indirect(reflect.ValueOf(object)).FieldByName(field)
}

func apply(subsystem subsystem, cgroup string, pid int) error {
	subsystemCgroupPath, error := getCgroupPath(subsystem.name(), cgroup, false)
	if error != nil {
		log.Println(error)
		return error
	}
	file := path.Join(subsystemCgroupPath, "tasks")
	if e := ioutil.WriteFile(file, []byte(strconv.Itoa(pid)), 0644); e != nil {
		log.Println(e)
		return e
	}
	return nil
}

func remove(subsystem subsystem, cgroup string) error {
	subsystemCgroupPath, error := getCgroupPath(subsystem.name(), cgroup, false)
	if error != nil {
		log.Println(error)
		return error
	}
	return os.RemoveAll(subsystemCgroupPath)
}

func getCgroupPath(subsystem string, cgroup string, makeIfNotExist bool) (string, error) {
	cgroupRoot, error := getCgroupRoot(subsystem)
	if error != nil {
		log.Println(error)
		return "", error
	}
	cgroupPath := path.Join(cgroupRoot, cgroup)
	if _, e := os.Stat(cgroupPath); e != nil {
		if !os.IsNotExist(e) || !makeIfNotExist {
			log.Println(e)
			return "", e
		}
		if e := os.Mkdir(cgroupPath, 0755); e != nil {
			log.Println(e)
			return "", e
		}
	}
	return cgroupPath, nil
}

func getCgroupRoot(subsystem string) (string, error) {
	file, error := os.Open("/proc/self/mountinfo")
	defer file.Close()
	if error != nil {
		log.Println(error)
		return "", error
	}
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		args := strings.Split(line, " ")
		options := args[len(args)-1]
		for _, option := range strings.Split(options, ",") {
			if option == subsystem {
				return args[4], nil
			}
		}
	}
	if e := scanner.Err(); e != nil {
		log.Println(e)
		return "", e
	}
	return "", errors.New("cgroup mount point not found")
}

var subsystems = []subsystem{cpuSetSubsystem{}, cpuSubsystem{}, memorySubsystem{}}
