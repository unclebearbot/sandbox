package main

import (
	"io/ioutil"
	"log"
	"strconv"

	"bitbucket.org/nvxarm/sandbox/command"
)

var version = "0"
var debug = "false"

func main() {
	command.Start(version)
}

func init() {
	if d, _ := strconv.ParseBool(debug); d {
		log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)
	} else {
		log.SetOutput(ioutil.Discard)
	}
}
