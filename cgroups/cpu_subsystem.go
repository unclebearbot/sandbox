package cgroups

type cpuSubsystem struct{}

func (cpuSubsystem cpuSubsystem) name() string {
	return "cpu"
}

func (cpuSubsystem cpuSubsystem) set(cgroup string, resource Resource) error {
	return set(cpuSubsystem, cgroup, "cpu.shares", resource, "CPU")
}

func (cpuSubsystem cpuSubsystem) apply(cgroup string, pid int) error {
	return apply(cpuSubsystem, cgroup, pid)
}

func (cpuSubsystem cpuSubsystem) remove(cgroup string) error {
	return remove(cpuSubsystem, cgroup)
}
