package cgroups

import "log"

type CgroupManager struct {
	Path     string
	Resource Resource
}

func NewCgroupManager(path string) CgroupManager {
	return CgroupManager{Path: path}
}

func (cgroupManager CgroupManager) Set(resource Resource) []error {
	errors := []error{}
	for _, subsystem := range subsystems {
		if e := subsystem.set(cgroupManager.Path, resource); e != nil {
			log.Println(e)
			errors = append(errors, e)
		}
	}
	return nilIfEmpty(errors)
}

func (cgroupManager CgroupManager) Apply(pid int) []error {
	errors := []error{}
	for _, subsystem := range subsystems {
		if e := subsystem.apply(cgroupManager.Path, pid); e != nil {
			log.Println(e)
			errors = append(errors, e)
		}
	}
	return nilIfEmpty(errors)
}

func (cgroupManager CgroupManager) Remove() []error {
	errors := []error{}
	for _, subsystem := range subsystems {
		if e := subsystem.remove(cgroupManager.Path); e != nil {
			log.Println(e)
			errors = append(errors, e)
		}
	}
	return nilIfEmpty(errors)
}

func nilIfEmpty(errors []error) []error {
	if len(errors) > 0 {
		return errors
	}
	return nil
}
