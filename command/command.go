package command

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"syscall"

	"bitbucket.org/nvxarm/sandbox/cgroups"
	"github.com/urfave/cli"
)

func Start(version string) {
	app := cli.NewApp()
	app.Version = version
	app.CustomAppHelpTemplate = `Version:
	{{.Version}}

Usage:
	sandbox run [option...] <command line>

Option:
	-i          interactive
	-s <value>  cpuset (default: 0)
	-c <value>  cpu (default: 1024)
	-m <value>  memory (default: 8m)

Example:
	sandbox run -i -s 0 -c 1024 -m 8m /bin/sh --posix
`
	app.Commands = []cli.Command{runCommand, initCommand}
	if e := app.Run(os.Args); e != nil {
		log.Println(e)
		os.Exit(1)
	}
}

var runCommand = cli.Command{
	Name:     "run",
	HideHelp: true,
	Flags: []cli.Flag{
		cli.BoolFlag{Name: "i", Usage: "interactive"},
		cli.StringFlag{Name: "s", Usage: "cpuset", Value: "0"},
		cli.StringFlag{Name: "c", Usage: "cpu", Value: "1024"},
		cli.StringFlag{Name: "m", Usage: "memory", Value: "8m"},
	},
	Action: func(context *cli.Context) error {
		if len(context.Args()) < 1 {
			return errors.New("missing command")
		}
		commandLine := context.Args()
		interactive := context.Bool("i")
		resource := cgroups.Resource{
			CPUSet: context.String("s"),
			CPU:    context.String("c"),
			Memory: context.String("m"),
		}
		if es := runProcess(interactive, commandLine, resource); es != nil {
			for e := range es {
				log.Println(e)
			}
			return es[0]
		}
		return nil
	},
}

func runProcess(interactive bool, commandLine []string, resource cgroups.Resource) []error {
	read, write, e := newPipe()
	if e != nil {
		log.Println(e)
		return []error{e}
	}
	process := exec.Command("/proc/self/exe", "init")
	process.SysProcAttr = &syscall.SysProcAttr{
		Cloneflags: syscall.CLONE_NEWIPC |
			syscall.CLONE_NEWNET |
			syscall.CLONE_NEWNS |
			syscall.CLONE_NEWPID |
			syscall.CLONE_NEWUTS,
	}
	if interactive {
		process.Stdin = os.Stdin
		process.Stdout = os.Stdout
		process.Stderr = os.Stderr
	}
	process.ExtraFiles = []*os.File{read}
	write.WriteString(strings.Join(commandLine, "\x00"))
	write.Close()
	if e := process.Start(); e != nil {
		log.Println(e)
		return []error{e}
	}
	cgroupManager := cgroups.NewCgroupManager("sandbox-" + strconv.Itoa(process.Process.Pid))
	defer cgroupManager.Remove()
	if errors := cgroupManager.Set(resource); errors != nil {
		return errors
	}
	if errors := cgroupManager.Apply(process.Process.Pid); errors != nil {
		return errors
	}
	if e := process.Wait(); e != nil {
		log.Println(e)
		return []error{e}
	}
	return nil
}

func newPipe() (*os.File, *os.File, error) {
	read, write, e := os.Pipe()
	if e != nil {
		log.Println(e)
		return nil, nil, e
	}
	return read, write, nil
}

var initCommand = cli.Command{
	Name:     "init",
	Hidden:   true,
	HideHelp: true,
	Action: func(context *cli.Context) error {
		return initProcess()
	},
}

func initProcess() error {
	commandLine, e := getCommandLine()
	if e != nil {
		log.Println(e)
		return e
	}
	if e := syscall.Mount("", "/", "", syscall.MS_PRIVATE|syscall.MS_REC, ""); e != nil {
		log.Println(e)
		return e
	}
	if e := syscall.Mount("proc", "/proc", "proc", syscall.MS_NODEV|syscall.MS_NOEXEC|syscall.MS_NOSUID, ""); e != nil {
		log.Println(e)
		return e
	}
	command := commandLine[0]
	return syscall.Exec(command, commandLine, nil)
}

func getCommandLine() ([]string, error) {
	pipe := os.NewFile(uintptr(3), "pipe")
	input, e := ioutil.ReadAll(pipe)
	if e != nil {
		if e.Error() == "read pipe: bad file descriptor" {
			fmt.Fprintln(os.Stderr, "No help topic for 'init'")
			os.Exit(1)
		}
		log.Println(e)
		return nil, e
	}
	return strings.Split(string(input), "\x00"), nil
}
