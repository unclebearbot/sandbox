package cgroups

type memorySubsystem struct{}

func (memorySubsystem memorySubsystem) name() string {
	return "memory"
}

func (memorySubsystem memorySubsystem) set(cgroup string, resource Resource) error {
	return set(memorySubsystem, cgroup, "memory.limit_in_bytes", resource, "Memory")
}

func (memorySubsystem memorySubsystem) apply(cgroup string, pid int) error {
	return apply(memorySubsystem, cgroup, pid)
}

func (memorySubsystem memorySubsystem) remove(cgroup string) error {
	return remove(memorySubsystem, cgroup)
}
